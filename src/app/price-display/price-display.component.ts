import { 
  Component, 
  Input 
} from '@angular/core';

/**
 * @PriceDisplay: A component to show the price of a 
 * Product
 */
@Component({
  selector: 'price-display',
  templateUrl: './price-display.component.html',
  styles: []
  // styleUrls: ['../app.component.scss']
})
export class PriceDisplayComponent {
  @Input() price: number;
}
