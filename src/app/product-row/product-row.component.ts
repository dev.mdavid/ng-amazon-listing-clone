import { 
  Component, 
  Input,
  HostBinding
} from '@angular/core';
import { Product } from '../product-model';

/**
 * @ProductRow: A component for the view of a single Product
 */
@Component({
  selector: 'product-row',
  templateUrl: './product-row.component.html',
  styles: []
  // styleUrls: ['../app.component.scss']
})
export class ProductRowComponent{
  @Input() product: Product;
  @HostBinding('attr.class') cssClass = 'item';
}